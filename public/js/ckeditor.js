// On initialise ckeditor (différentes selon la version choisi ici balloon)

BalloonEditor
    .create(
        document.querySelector("#editor"), {
            //toolbar: []
        }
    )
    .then(editor => {
        //console.log(editor)
            editor.sourceElement.parentElement.addEventListener("submit", function(e){
            e.preventDefault();
            this.querySelector("#editor + input").value = editor.getData()
            this.submit()
        })
    })