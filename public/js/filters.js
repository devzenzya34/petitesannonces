/* Gerer les clicks des input dans le filtres */
window.onload = () => {
    const FiltersForm = document.querySelector("#filters")
    //recupère ts les input
    document.querySelectorAll("#filters input")
        .forEach(input => {
            input.addEventListener("change", () => {
                // ici on intercepte les click
                //on récup les données du formulaire via FORMDATA
                const Form = new FormData(FiltersForm);

                //on fabirque la querystring(paramètres d'url)
                const Params = new URLSearchParams();

                Form.forEach((value, key) => {
                    Params.append(key, value); //genere les paramètres de l'url
                })

                //on récupère l'url active et on crée la requete ajax
                const Url = new URL(window.location.href)
                //console.log(url)

                //on lance la requete AJAX (si le paramètre ajax=1 nécessaire pour la route index qui gère aussi des requetes sans ajax)
                fetch(Url.pathname + "?" + Params.toString() + "&ajax=1", {
                    headers: {
                        "X-Requested-width": "XMLHttpRequest"
                    }
                }).then(response =>
                    response.json()
                ).then(data => {
                    //on va chercher la zone de contenu et on remplace par les data filtrées
                    const content = document.querySelector("#home_content");
                    content.innerHTML = data.content;

                    //on met à jour l'url (pour pouvoir gérer la pagination avec les filtres)
                    history.pushState({}, null, Url.pathname + "?" + Params.toString())
                }).catch(e => alert(e))
            });
        });
}