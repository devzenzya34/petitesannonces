<?php

namespace App\Controller;

use App\Form\MainContactType;
use App\Form\SearchAnnonceType;
use App\Repository\AnnoncesRepository;
use App\Repository\CategoriesRepository;
use App\Service\SendMailService;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     */
    public function index(AnnoncesRepository $annoncesRepo, CategoriesRepository $catRepo, Request $request)
    {
        //$annonces = $annoncesRepo->findBy(['active' => true], ['created_at' => 'desc'], 5);
        /* Afficher toutes les annonces avec pagination
            Ajout d'un filtre pour les catégories en AJAX */
        // On définit le nombre d'éléments par page
        $limit = 5;
        // On récupère le numéro de page
        $page = (int)$request->query->get("page", 1);
        //on récupère les filtres
        $filters = $request->get("categories");

        // On récupère les annonces de la page en fonction du filtre
        $annonces = $annoncesRepo->getPaginatedAnnonces($page, $limit, $filters);
        //dd($annonces);
        // On récupère le nombre total d'annonces
        $total = $annoncesRepo->getTotalAnnonces($filters);
        //on verifie si on a une requête ajax
        if($request->get('ajax')) {
            return new JsonResponse([
                'content' => $this->renderView('main/_home.html.twig', compact('annonces', 'total', 'limit', 'page'))
            ]);
        }
        //dd($total);

        // SEARCH FORM
        $form = $this->createForm(SearchAnnonceType::class);
        
        $search = $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            // On recherche les annonces correspondant aux mots clés
            $annonces = $annoncesRepo->search(
                $search->get('mots')->getData(),
                $search->get('categorie')->getData()
            );
        }

        //Filtre AJAX selon la catégorie
        $categories = $catRepo->findAll();

        return $this->render('main/index.html.twig', [
            'annonces' => $annonces,
            'total' => $total,
            'limit' => $limit,
            'page' => $page,
            'categories' => $categories,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/mentions/legales", name="mentions")
     */
    public function mentions()
    {
        return $this->render('main/mentions.html.twig');
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contact(Request $request, SendMailService $mailService)
    {
        $form = $this->createForm(MainContactType::class);

        $contact = $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            // Envoie de mail (refacto par une création de service send mail)
            $context = [
                'mail' => $contact->get('email')->getData(),
                'sujet' => $contact->get('sujet')->getData(),
                'message' => $contact->get('message')->getData(),
            ];
            $mailService->send(
                $contact->get('email')->getData(),
                'vous@domaine.fr',
                'Contact depuis le site PetitesAnnonces',
                'contact',
                $context
            );
            /*$email = (new TemplatedEmail())
                ->from($contact->get('email')->getData())
                ->to('vous@domaine.fr')
                ->subject('Contact depuis le site PetitesAnnonces')
                ->htmlTemplate('emails/contact.html.twig')
                ->context([
                    'mail' => $contact->get('email')->getData(),
                    'sujet' => $contact->get('sujet')->getData(),
                    'message' => $contact->get('message')->getData(),
                ])
            ;
            $mailer->send($email);*/

            $this->addFlash('message', 'Mail de contact envoyé');
            return $this->redirectToRoute('contact');
        }

        return $this->render('main/contact.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
