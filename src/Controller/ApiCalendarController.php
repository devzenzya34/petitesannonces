<?php

namespace App\Controller;

use App\Entity\Calendar;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiCalendarController extends AbstractController
{
    /**
     * @Route("/api/calendar", name="api_calendar")
     */
    public function index(): Response
    {
        return $this->render('api_calendar/index.html.twig', [
            'controller_name' => 'ApiCalendarController',
        ]);
    }

    /**
     * Route pour éditer un évenement sur le calendrier: changement d'un paramètre du rdv
     * @Route("/api/{id}/edit", name="api_calendar_event_edit", methods={"PUT"})
     */
    public function majCalendarEvent(?Calendar $calendar, Request $request): Response
    {
        //On récupère les données envoyées par fullCalendar
        $donnees = json_decode($request->getContent());
        if(
            isset($donnees->title) && !empty($donnees->title) &&
            isset($donnees->start) && !empty($donnees->start) &&
            isset($donnees->description) && !empty($donnees->description) &&
            isset($donnees->backgroundColor) && !empty($donnees->backgroundColor) &&
            isset($donnees->borderColor) && !empty($donnees->borderColor) &&
            isset($donnees->textColor) && !empty($donnees->textColor)
        ) {
            //les données sont complètes
            //Initialisation d'un code response
            $code = 200;

            //on vérifie si l'id existe
            if(!$calendar) {
                //on instancie le rdv
                $calendar = new Calendar;
                $code = 201;
            }
            //On hydrate l'objet avec les données
            $calendar->setTitle($donnees->title);
            $calendar->setDescription($donnees->description);
            $calendar->setAllDay($donnees->allDay);
            $calendar->setBackgroundColor($donnees->backgroundColor);
            $calendar->setBorderColor($donnees->borderColor);
            $calendar->setStart(new \DateTime($donnees->start));
            if($donnees->allDay){
                $calendar->setEnd(new \DateTime($donnees->start));
            } else {
                $calendar->setEnd(new \DateTime($donnees->end));
            }
            $calendar->setTextColor($donnees->textColor);

            $em = $this->getDoctrine()->getManager();
            $em->persist($calendar);
            $em->flush();

            //on retourne le code
            return new Response('Données Transmises', $code);

        } else {
            //les données sont incomplètes
            return new Response('Données incomplètes', 404);
        }

        return $this->render('api_calendar/index.html.twig', [
            'controller_name' => 'ApiCalendarController',
        ]);
    }
}
