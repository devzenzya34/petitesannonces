<?php

namespace App\Controller;

use App\Entity\Annonces;
use App\Entity\Comments;
use App\Entity\Images;
use App\Form\AnnonceContactType;
use App\Form\AnnoncesType;
use App\Form\CommentsType;
use App\Repository\AnnoncesRepository;
use App\Service\ManagePictureService;
use App\Service\SendMailService;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/annonces", name="annonces_")
 * @package App\Controller
 */
class AnnoncesController extends AbstractController
{
    // TODO: Fonction index sera une fonction qui affichera les annocnes selon le id de l'user liés à mes annonces dans le compte profil
    /**
     * @Route("/", name="home")
     * @return void
     */
    public function index(AnnoncesRepository $annoncesRepo, Request $request)
    {
        // On définit le nombre d'éléments par page
        $limit = 10;

        // On récupère le numéro de page
        $page = (int)$request->query->get("page", 1);

        // On récupère les annonces de la page
        $annonces = $annoncesRepo->getPaginatedAnnonces($page, $limit);

        // On récupère le nombre total d'annonces
        $total = $annoncesRepo->getTotalAnnonces();

        return $this->render('annonces/index.html.twig', compact('annonces', 'total', 'limit', 'page'));
    }

    /**
     * @Route("/ajout", name="ajout")
     */
    public function ajoutAnnonce(Request $request, ManagePictureService $managePictureService)
    {
        $annonce = new Annonces;

        $form = $this->createForm(AnnoncesType::class, $annonce);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $annonce->setUsers($this->getUser());
            $annonce->setActive(false);
            // On récupère les images transmises
            $images = $form->get('images')->getData();

            //Ajout des images via le service
            //$managePictureService->addImages($images, $annonce);

            // On boucle sur les images
            foreach ($images as $image) {
                // On génère un nouveau nom de fichier
                $fichier = md5(uniqid()) . '.' . $image->guessExtension();

                // On copie le fichier dans le dossier uploads
                $image->move(
                    $this->getParameter('images_directory'),
                    $fichier
                );

                // On crée l'image dans la base de données
                $img = new Images();
                $img->setName($fichier);
                $annonce->addImage($img);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($annonce);
            $em->flush();

            return $this->redirectToRoute('app_home');
        }

        return $this->render('annonces/ajout.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/edit/{id}", name="edit")
     */
    public function editAnnonce(Annonces $annonce, Request $request, ManagePictureService $managePictureService)
    {
        //restreint l'edition de l'annonce si on est pas propriétaire
        $this->denyAccessUnlessGranted('annonce_edit', $annonce);

        $form = $this->createForm(AnnoncesType::class, $annonce);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $annonce->setActive(false);
            // On récupère les images transmises
            $images = $form->get('images')->getData();

            // Refacto par un service gestion d' images
            $managePictureService->addImages($images, $annonce);

             /* // On boucle sur les images
            foreach ($images as $image) {
                // On génère un nouveau nom de fichier
                $fichier = md5(uniqid()) . '.' . $image->guessExtension();

                // On copie le fichier dans le dossier uploads
                $image->move(
                    $this->getParameter('images_directory'),
                    $fichier
                );

                // On crée l'image dans la base de données
                $img = new Images();
                $img->setName($fichier);
                $annonce->addImage($img);
            }*/
            $em = $this->getDoctrine()->getManager();
            $em->persist($annonce);
            $em->flush();

            return $this->redirectToRoute('annonces_home');
        }

        return $this->render('annonces/ajout.html.twig', [
            'form' => $form->createView(),
            'annonce' => $annonce
        ]);
    }

    /**
     * @Route("/details/{slug}", name="details")
     */
    public function details($slug, AnnoncesRepository $annoncesRepo, Request $request, SendMailService $mailService)
    {
        $annonce = $annoncesRepo->findOneBy(['slug' => $slug]);

        if (!$annonce) {
            throw new NotFoundHttpException('Pas d\'annonce trouvée');
        }

        //Formulaire d'envoie de message à l'auteur (modal)
        $form = $this->createForm(AnnonceContactType::class);

        $contact = $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Envoie de mail (refacto par une création de service send mail)
            $context = [
                'annonce' => $annonce,
                'mail' => $contact->get('email')->getData(),
                'message' => $contact->get('message')->getData()
            ];
            $mailService->send(
                $contact->get('email')->getData(),
                $annonce->getUsers()->getEmail(),
                'Contact au sujet de votre annonce "' . $annonce->getTitle() . '"',
                'contact_annonce',
                $context
            );
            // On crée le mail (sans le service sendmail)
            /*$email = (new TemplatedEmail())
                ->from($contact->get('email')->getData())
                ->to($annonce->getUsers()->getEmail())
                ->subject('Contact au sujet de votre annonce "' . $annonce->getTitle() . '"')
                ->htmlTemplate('emails/contact_annonce.html.twig')
                ->context([
                    'annonce' => $annonce,
                    'mail' => $contact->get('email')->getData(),
                    'message' => $contact->get('message')->getData()
                ]);
            // On envoie le mail
            $mailer->send($email);*/

            // On confirme et on redirige
            $this->addFlash('message', 'Votre e-mail a bien été envoyé');
            return $this->redirectToRoute('annonces_details', ['slug' => $annonce->getSlug()]);
        }

        //PARTIE COMMENTAIRE
        $comment = new Comments;

        $commentForm = $this->createForm(CommentsType::class, $comment);

        $commentForm->handleRequest($request);

        if ($commentForm->isSubmitted() && $commentForm->isValid()) {
            $comment->setCreatedAt(new \DateTimeImmutable());
            $comment->setAnnonces($annonce);

            //on récupère le contenu de parentid
            $parentid = $commentForm->get("parentid")->getData();
            //dd($parentid);
            //on va chercher le commentaire correspondant
            $em = $this->getDoctrine()->getManager();
            if ($parentid != null) {
                $parent = $em->getRepository(Comments::class)->find($parentid);
            }
            //on définit le parent
            $comment->setParent($parent ?? null);

            $em->persist($comment);
            $em->flush();

            $this->addFlash('message', 'Votre commentaire a bien été envoyé!!');
            return $this->redirectToRoute('annonces_details', ['slug' => $annonce->getSlug()]);
        }


        return $this->render('annonces/details.html.twig', [
            'annonce' => $annonce,
            'form' => $form->createView(),
            'commentForm' => $commentForm->createView()
        ]);
    }

    /**
     * @Route("/supprimer/{id}", name="supprimer")
     */
    public function supprimer(Annonces $annonce)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($annonce);
        $em->flush();

        $this->addFlash('message', 'Annonce supprimée avec succès');

        return $this->redirectToRoute('app_home');
    }

    /**
     * @Route("/favoris/ajout/{id}", name="ajout_favoris")
     */
    public function ajoutFavoris(Annonces $annonce)
    {
        if (!$annonce) {
            throw new NotFoundHttpException('Pas d\'annonce trouvée');
        }
        $annonce->addFavori($this->getUser());

        $em = $this->getDoctrine()->getManager();
        $em->persist($annonce);
        $em->flush();
        return $this->redirectToRoute('app_home');
    }

    /**
     * @Route("/favoris/retrait/{id}", name="retrait_favoris")
     */
    public function retraitFavoris(Annonces $annonce)
    {
        if (!$annonce) {
            throw new NotFoundHttpException('Pas d\'annonce trouvée');
        }
        $annonce->removeFavori($this->getUser());

        $em = $this->getDoctrine()->getManager();
        $em->persist($annonce);
        $em->flush();
        return $this->redirectToRoute('app_home');
    }

    /**
     * @Route("/supprime/image/{id}", name="delete_image", methods={"DELETE"})
     */
    public function deleteImage(Images $image, Request $request)
    {
        $data = json_decode($request->getContent(), true);

        // On vérifie si le token est valide
        if ($this->isCsrfTokenValid('delete' . $image->getId(), $data['_token'])) {
            // On récupère le nom de l'image
            $nom = $image->getName();
            // On supprime le fichier
            unlink($this->getParameter('images_directory') . '/' . $nom);

            // On supprime l'entrée de la base
            $em = $this->getDoctrine()->getManager();
            $em->remove($image);
            $em->flush();

            // On répond en json
            return new JsonResponse(['success' => 1]);
        } else {
            return new JsonResponse(['error' => 'Token Invalide'], 400);
        }
    }
}
