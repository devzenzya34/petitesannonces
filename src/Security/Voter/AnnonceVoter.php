<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class AnnonceVoter extends Voter
{
    const ANNONCE_EDIT = "annonce_edit";
    const ANNONCE_DELETE = "annonce_delete";

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    //verifie si les demandes de permissions existent au niveau du voter
    protected function supports(string $attribute, $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [self::ANNONCE_DELETE, self::ANNONCE_EDIT])
            && $subject instanceof \App\Entity\Annonces;
    }

    //verifie si on respecte les critères du voter
    protected function voteOnAttribute(string $attribute, $annonce, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        //on verifie si le user est admin
        if($this->security->isGranted('ROLE_ADMIN')) return true;

        //on vérifie si l'annonce a un propriétaire
        if (null === $annonce->getUsers()) return false;


        // ... (check conditions and return true to grant permission) l'annonce a unh propriétaire...
        switch ($attribute) {
            case self::ANNONCE_DELETE:
                // logic to determine if the user can EDIT
                return $this->canEdit($annonce, $user);
                break;
            case self::ANNONCE_DELETE:
                // logic to determine if the user can VIEW
                return $this->canDelete($annonce, $user);
                break;
        }
        return false;
    }

    private function canEdit()
    {
        return $user === $annonce->getUsers();
    }

    private function canDelete()
    {
        return $user === $annonce->getUsers();
    }
}
